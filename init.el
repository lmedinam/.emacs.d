;; init.el - Luis Medina's Emacs configuration file

;; Author: Luis Medina <luismedina.git@outlook.com>
;; URL: https://gitlab.com/medinam/emacs.d

;; USE MELPA AND INSTALL USE-PACKAGE
;; ---------------------------------

(require 'package)

(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)

(package-initialize)

;; No packages? Then refresh
(when (not package-archive-contents)
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

;; PACKAGE INSTALLATION AND CONFIGURATION
;; --------------------------------------

(use-package diminish
  :ensure t)

(use-package leuven-theme
  :ensure t
  :config (load-theme 'leuven t))

(use-package ido-completing-read+
  :ensure t
  :init
  (progn
    (ido-mode t)
    (use-package smex
      :ensure t
      :init (smex-initialize)
      :bind ("M-x" . smex))))

(use-package rainbow-delimiters
  :ensure t
  :config
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

(use-package clojure-mode :ensure t)

(use-package clojure-mode-extra-font-locking :ensure t)

(use-package cider
  :ensure t
  :config
  (setq cider-lein-command "~/bin/lein"))

(use-package paredit
  :ensure t
  :diminish paredit-mode
  :hook ((clojure-mode . paredit-mode)
         (cider-repl-mode . paredit-mode)
         (lisp-mode . paredit-mode)
         (emacs-lisp-mode . paredit-mode)
         (lisp-interaction-mode . paredit-mode)))

(use-package yasnippet
  :ensure t
  :init (add-hook 'prog-mode-hook #'yas-minor-mode)
  :config (yas-reload-all))

(use-package clojure-snippets :ensure t)

(use-package aggressive-indent
  :ensure t
  :config
  (global-aggressive-indent-mode 1)
  (add-to-list 'aggressive-indent-excluded-modes 'sql-mode))

(use-package web-mode
  :ensure t
  :mode (("\\.html\\'" . web-mode)
         ("\\.css\\'" . web-mode)
         ("\\.js\\'" . web-mode)
         ("\\.php\\'" . web-mode)
         ("\\.vue\\'" . web-mode))
  :init
  (progn
    (defun my-web-mode-hook ()
      (setq-default indent-tabs-mode nil))
    (add-hook 'web-mode-hook 'my-web-mode-hook)
    (setq web-mode-engines-alist
          '(("django" . "\\.html\\'")
            ("blade" . "\\.blade\\.")))))

(use-package company
  :ensure t
  :init (global-company-mode))

(use-package projectile
  :ensure t
  :diminish projectile-mode
  :config (projectile-global-mode))

(use-package groovy-mode
  :ensure t
  :mode (("\\.gson\\'" . groovy-mode)))

(use-package yaml-mode
  :ensure t
  :mode (("\\.yml\\'" . yaml-mode)))

(use-package smartparens
  :ensure t
  :diminish smartparens-mode
  :config
  (progn
    (require 'smartparens-config))
  :hook ((groovy-mode . smartparens-mode)
         (yaml-mode . smartparens-mode)
         (web-mode . smartparens-mode)
         (less-css-mode . smartparens-mode)))

(use-package magit
  :ensure t
  :bind (("C-x g" . magit-status)))

(use-package scss-mode
  :ensure t
  :config (setq css-indent-offset 2))

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

(use-package gdscript-mode
  :ensure t)

(use-package column-enforce-mode
  :ensure t
  :hook (prog-mode . column-enforce-mode))

(use-package less-css-mode
  :ensure t)

;; CUSTOM FUNCTIONS
;; ----------------

(defun kill-other-buffers ()
  "Kill all other buffers."
  (interactive)
  (mapc 'kill-buffer (delq (current-buffer) (buffer-list))))

;; BASIC CUSTOMIZATION
;; -------------------

(show-paren-mode 1) ;; Show matching pairs of parentheses

(menu-bar-mode -1) ;; Disable the menu bar
(toggle-scroll-bar -1) ;; Disable the scrollbar
(tool-bar-mode -1) ;; Disable the toolbar

(blink-cursor-mode 0) ;; Disable blink cursor
(setq-default cursor-type 'hbar) ;; Set cursor to a horizontal bar

(setq-default indent-tabs-mode nil) ;; Never use tabs for indent
(setq inhibit-startup-message t) ;; Hide the startup message
(setq column-number-mode t) ;; Enable colmn number globally
(global-linum-mode t) ;; Enable line numbers globally

(setq make-backup-files nil) ; Stop creating backup~ files
(setq auto-save-default nil) ; Stop creating #autosave# files
(setq x-select-enable-clipboard t) ;; Interact with the clipboard

(setq-default ispell-program-name "aspell")
(setq ispell-dictionary "castellano")

(setq-default buffer-file-coding-system 'utf-8-unix)

;; CUSTOM-SET-VARIABLES
;; --------------------

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (paredit-everywhere web-mode web use-package smex rainbow-mode rainbow-delimiters paredit leuven-theme ido-completing-read+ company clojure-snippets cider))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
